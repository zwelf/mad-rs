use crate::{Bridge, DiscordEvent, MatrixEvent};
use serde::{Deserialize, Serialize};
use serenity::async_trait;
use serenity::http::Http;
use serenity::model::channel::Message;
use serenity::model::gateway::Ready;
use serenity::model::id::{ChannelId, UserId};
use serenity::model::webhook::Webhook;
use serenity::prelude::*;
use std::collections::HashMap;
use std::panic;
use tokio::sync::mpsc;
use tokio::{select, spawn};
use tracing::{info, warn};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DiscordConfig {
    pub application_id: String,
    pub secret_token: String,
}

#[derive(Debug)]
pub struct Discord {
    config: DiscordConfig,
}

struct Handler {
    tx: mpsc::UnboundedSender<DiscordEvent>,
}

#[async_trait]
impl EventHandler for Handler {
    // Set a handler for the `message` event - so that whenever a new message
    // is received - the closure (or function) passed will be called.
    //
    // Event handlers are dispatched through a threadpool, and so multiple
    // events can be dispatched simultaneously.
    async fn message(&self, ctx: Context, msg: Message) {
        println!("discord shard: '{:?}', message: '{:?}'", ctx.shard, msg);
        self.tx
            .send(DiscordEvent { msg })
            .expect("Matrix Receiver closed");
    }

    // Set a handler to be called on the `ready` event. This is called when a
    // shard is booted, and a READY payload is sent by Discord. This payload
    // contains data like the current user's guild Ids, current user data,
    // private channels, and more.
    //
    // In this case, just print what the current user's username is.
    async fn ready(&self, _: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);
    }
}

impl Discord {
    pub fn new(config: DiscordConfig) -> Self {
        Discord { config }
    }

    async fn channel_id_webhook(http: &Http, bridges: &[Bridge]) -> HashMap<ChannelId, Webhook> {
        let mut webhooks = HashMap::new();
        for bridge in bridges {
            'channel: for channel in bridge.relays.iter() {
                let channel_id = channel.channel_id;
                if webhooks.contains_key(&channel_id) {
                    panic!("channel_id {} appeared twice", channel_id);
                }

                let channel_webhooks = channel_id.webhooks(&http).await.unwrap();
                for w in channel_webhooks {
                    info!("Reusing webhook {w:?} for channel {channel_id}");
                    assert_eq!(w.user.as_ref().unwrap().id, UserId(1062722321833533440));
                    webhooks.insert(channel_id, w);
                    continue 'channel;
                }

                info!("Creating new webhook for channel {channel_id}");
                let new_webhook = channel_id.create_webhook(&http, "mad-rs").await.unwrap();
                webhooks.insert(channel_id, new_webhook);
            }
        }
        webhooks
    }

    async fn on_matrix_messages(
        http: Http,
        mut rx: mpsc::UnboundedReceiver<MatrixEvent>,
        bridges: &[Bridge],
    ) -> Result<(), ()> {
        let webhooks = Discord::channel_id_webhook(&http, bridges).await;
        while let Some(el) = rx.recv().await {
            println!("Executing webhook");
            let w = webhooks.get(&ChannelId::from(el.channel_id)).unwrap();
            match w.execute(&http, false, el.webhook).await {
                Ok(ok) => info!("send webhook {ok:?}"),
                Err(err) => warn!("failed to execute webhook: {err:?}"),
            }
        }
        warn!("Discord forwarder stopped");
        Err(())
    }

    pub async fn listen_discord(
        secret_token: &str,
        tx: mpsc::UnboundedSender<DiscordEvent>,
    ) -> Result<(), ()> {
        // Set gateway intents, which decides what events the bot will be notified about
        let intents = GatewayIntents::all();

        // Create a new instance of the Client, logging in as a bot. This will
        // automatically prepend your bot token with "Bot ", which is a requirement
        // by Discord for bot users.
        let mut client = Client::builder(secret_token, intents)
            .event_handler(Handler { tx })
            .await
            .expect("Err creating client");

        // Finally, start a single shard, and start listening to events.
        //
        // Shards will automatically attempt to reconnect, and will perform
        // exponential backoff until it reconnects.
        client.start().await.unwrap();
        Err(())
    }

    pub async fn start_bridge(
        &mut self,
        bridges: &[Bridge],
        rx: mpsc::UnboundedReceiver<MatrixEvent>,
        tx: mpsc::UnboundedSender<DiscordEvent>,
    ) -> Result<(), ()> {
        let bridges = bridges.to_vec();
        let secret_token = self.config.secret_token.clone();

        let task1 = spawn(async move {
            let http = Http::new(&secret_token);
            Self::on_matrix_messages(http, rx, &bridges).await
        });
        let secret_token = self.config.secret_token.clone();
        let task2 = spawn(async move { Self::listen_discord(&secret_token, tx).await });
        select!(
            result = task1 => {
                result.unwrap().unwrap();
            }
            result = task2 => {
                result.unwrap().unwrap();
            }
        );
        Err(())
    }
}
