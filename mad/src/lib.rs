use matrix_sdk_appservice::ruma::{OwnedRoomAliasId, OwnedUserId};
use serde::{Deserialize, Serialize};
use serenity::builder::ExecuteWebhook;
use serenity::model::channel::Message;
use serenity::model::id::{ChannelId, GuildId};
use std::fmt;
use std::fmt::Formatter;

mod discord;
mod matrix;

pub use discord::{Discord, DiscordConfig};
pub use matrix::{Matrix, MatrixConfig};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Relay {
    name: String,
    ordering: String,
    channel_id: ChannelId,
    room: OwnedRoomAliasId,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Space {
    name: String,
    guild_id: GuildId,
    /// Space, which discord bot users join in order to be able to join bridged rooms
    internal_space: OwnedRoomAliasId,
    /// Space which matrix users have to join in order to join the bridged rooms
    public_space: OwnedRoomAliasId,
    /// User to invite and elevate to admin rights
    admin: Option<OwnedUserId>,
    /// User to invite and elevate to moderator rights
    moderator: Option<OwnedUserId>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Bridge {
    space: Space,
    /// statically configured list of rooms to bridge
    relays: Vec<Relay>,
}

#[derive(Clone, Debug)]
pub struct DiscordEvent {
    msg: Message,
}

pub struct MatrixEvent {
    pub channel_id: u64,
    pub webhook: Box<
        dyn for<'a, 'b> FnOnce(&'b mut ExecuteWebhook<'a>) -> &'b mut ExecuteWebhook<'a>
            + Send
            + 'static,
    >,
}

impl fmt::Debug for MatrixEvent {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "MatrixEvent {{ channel_id: {:?}, webhook: ptr }}",
            self.channel_id
        )
    }
}
