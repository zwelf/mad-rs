use matrix_sdk_appservice::matrix_sdk::config::SyncSettings;
use matrix_sdk_appservice::{AppService, AppServiceRegistration, UserBuilder};
use std::time::Duration;

#[tokio::main]
async fn main() {
    let registration = AppServiceRegistration::try_from_yaml_file("registration.yaml").unwrap();

    let appservice = AppService::builder(
        "http://localhost:6867".try_into().unwrap(),
        "ddnet.org".try_into().unwrap(),
        registration,
    )
    .build()
    .await
    .unwrap();

    let _ = appservice.register_user("_testing_bot", None).await;
    // let user = appservice.user(Some("_testing_bot")).await.unwrap();
    let user = UserBuilder::new(&appservice, "_testing_bot")
        .device_id(Some("testing-rs".into()))
        .login()
        .build()
        .await
        .unwrap();

    let sync_settings = SyncSettings::new().timeout(Duration::from_millis(0));
    user.sync_once(sync_settings).await.unwrap();
    for room in user.invited_rooms() {
        room.accept_invitation().await.unwrap();
    }
}
