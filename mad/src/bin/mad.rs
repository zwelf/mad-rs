use clap::Parser;
use mad_rs::{Bridge, Discord, DiscordConfig, Matrix, MatrixConfig};
use serde::{Deserialize, Serialize};
use std::fs;
use tokio::select;
use tokio::sync::mpsc;
use tracing::{info, Level};
use tracing_subscriber::FmtSubscriber;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
struct Cli {
    /// Sets a custom config file
    #[arg(short, long, value_name = "FILE", default_value_t = String::from("mad.yaml"))]
    config: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct Config {
    discord: DiscordConfig,
    matrix: MatrixConfig,
    bridges: Vec<Bridge>,
}

#[tokio::main]
async fn main() {
    let cli: Cli = Cli::parse();
    // a builder for `FmtSubscriber`.
    let subscriber = FmtSubscriber::builder()
        // all spans/events with a level higher than TRACE (e.g, debug, info, warn, etc.)
        // will be written to stdout.
        .with_max_level(Level::TRACE)
        // completes the builder.
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");
    info!("mad startup");

    let config = fs::read_to_string(cli.config).unwrap();
    let config: Config = serde_yaml::from_str(&config).unwrap();

    info!("config {config:?}");

    let (matrix_tx, discord_rx) = mpsc::unbounded_channel();
    let (discord_tx, matrix_rx) = mpsc::unbounded_channel();

    let mut discord = Discord::new(config.discord);
    let discord = discord.start_bridge(&config.bridges, discord_rx, discord_tx);
    let mut matrix = Matrix::new(config.matrix);
    let matrix = matrix.start_bridge(&config.bridges, matrix_rx, matrix_tx);
    select!(
        _ = discord => panic!("Completed"),
        _ = matrix => panic!("Completed"),
    );
}
