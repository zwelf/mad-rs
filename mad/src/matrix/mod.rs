use crate::{Bridge, DiscordEvent, MatrixEvent};
use matrix_sdk_appservice::matrix_sdk::event_handler::Ctx;
use matrix_sdk_appservice::matrix_sdk::reqwest::Url;
use matrix_sdk_appservice::matrix_sdk::room::{Joined, RoomMember};
use matrix_sdk_appservice::matrix_sdk::Client;
use matrix_sdk_appservice::ruma::api::client::room::create_room;
use matrix_sdk_appservice::ruma::api::client::room::create_room::v3::CreationContent;
use matrix_sdk_appservice::ruma::events::room::join_rules::JoinRule;
use matrix_sdk_appservice::ruma::events::room::message::{
    MessageType, RoomMessageEventContent, SyncRoomMessageEvent, TextMessageEventContent,
};
use matrix_sdk_appservice::ruma::events::room::{join_rules, power_levels};
use matrix_sdk_appservice::ruma::events::{space, EmptyStateKey, InitialStateEvent};
use matrix_sdk_appservice::ruma::room::RoomType;
use matrix_sdk_appservice::ruma::serde::Raw;
use matrix_sdk_appservice::ruma::{OwnedServerName, RoomAliasId, UserId};
use matrix_sdk_appservice::{matrix_sdk, AppService, AppServiceRegistration, UserBuilder};
use serde::{Deserialize, Serialize};
use serenity::model::id::{ChannelId, GuildId};
use std::collections::HashMap;
use std::path::PathBuf;
use tokio::sync::mpsc;
use tokio::{select, spawn};
use tracing::{error, warn};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MatrixConfig {
    pub homeserver_url: Url,
    pub server_name: OwnedServerName,
    pub appservice_registration_file: PathBuf,
}

#[derive(Debug)]
pub struct Matrix {
    config: MatrixConfig,
}

pub struct Puppet {
    client: Client,
    guilds: HashMap<GuildId, Joined>,
    channel: HashMap<ChannelId, Joined>,
}

impl Puppet {
    fn new(client: Client) -> Self {
        Self {
            client,
            guilds: HashMap::new(),
            channel: HashMap::new(),
        }
    }
}

struct Room {
    user: Client,
    room: Joined,
    puppets: HashMap<u64, Puppet>,
    /// created in this session
    created: bool,
}

impl Room {
    async fn invite_moderator(
        &mut self,
        moderator: &UserId,
        members: &[RoomMember],
        set_power_level: i64,
    ) {
        let mut update_power = false;
        if let Some(m) = members.iter().find(|el| el.user_id() == moderator) {
            if m.power_level() < set_power_level {
                update_power = true;
            }
        } else {
            // invite user
            if let Err(err) = self.room.invite_user_by_id(moderator).await {
                warn!("err {err}");
                return;
            }
            update_power = true;
        }
        if update_power {
            let mut power_level = power_levels::RoomPowerLevelsEventContent::new();
            power_level
                .users
                .insert(moderator.into(), set_power_level.try_into().unwrap());
            //self.room.send_state_event(power_level).await.unwrap();
        }
    }

    pub async fn setup(
        appservice: &AppService,
        user: Client,
        room_alias: &RoomAliasId,
        name: String,
        admin: Option<&UserId>,
        moderator: Option<&UserId>,
        join_rule: Option<&JoinRule>,
        space: bool,
    ) -> Room {
        // get already joined room or create
        let (joined, created) = match user.resolve_room_alias(room_alias).await {
            Ok(response) => (
                user.join_room_by_id(&response.room_id).await.unwrap(),
                false,
            ),
            Err(_err) => {
                println!("creating alias {room_alias:?}");
                let mut request = create_room::v3::Request::new();
                request.room_alias_name = Some(room_alias.alias().to_owned());
                request.name = Some(name);
                request.invite.push(user.user_id().unwrap().into());
                if space {
                    let mut creation_content = CreationContent::new();
                    creation_content.room_type = Some(RoomType::Space);
                    request.creation_content = Some(Raw::new(&creation_content).unwrap());
                }

                let mut power_level = power_levels::RoomPowerLevelsEventContent::new();
                // invite moderator/admin
                power_level.invite = 50.into();
                power_level.events_default = 10.into();
                let power_level = InitialStateEvent {
                    content: power_level,
                    state_key: EmptyStateKey,
                };
                request
                    .initial_state
                    .push(Raw::new(&power_level).unwrap().cast());

                // space join rules
                if let Some(join_rule) = join_rule {
                    let join_rule = join_rules::RoomJoinRulesEventContent::new(join_rule.clone());
                    let join_rule = InitialStateEvent {
                        content: join_rule,
                        state_key: EmptyStateKey,
                    };
                    request
                        .initial_state
                        .push(Raw::new(&join_rule).unwrap().cast());
                }

                (user.create_room(request).await.unwrap(), true)
            }
        };
        let mut room = Self {
            user,
            room: joined,
            puppets: HashMap::new(),
            created,
        };

        let members = room.room.members().await.unwrap();

        // invite and set power level of admin/moderator
        if let Some(admin) = admin {
            room.invite_moderator(admin, &members, 100).await;
        }
        if let Some(moderator) = moderator {
            room.invite_moderator(moderator, &members, 50).await;
        }

        // look up all already existing puppets in this room
        for m in members {
            // puppets have to be on the same server and matching a regex
            if m.user_id().server_name() != room.user.user_id().unwrap().server_name() {
                continue;
            }
            if !appservice.user_id_is_in_namespace(m.user_id()) {
                continue;
            }
            let discord_id = m.user_id().localpart();
            let id_start = discord_id.rfind(|c: char| !c.is_ascii_digit()).unwrap() + 1;
            // skip all users not ending with an id (the bot user)
            if id_start == discord_id.len() {
                continue;
            }
            let discord_id: u64 = discord_id[id_start..].parse().unwrap();

            let client = appservice
                .user(Some(m.user_id().localpart()))
                .await
                .unwrap();
            room.puppets.insert(discord_id, Puppet::new(client));
        }

        room
    }

    pub async fn on_discord_event(&mut self, event: DiscordEvent) {
        let content = TextMessageEventContent::plain(event.msg.content);
        let msg = RoomMessageEventContent::new(MessageType::Text(content));
        self.room.send(msg, None).await.unwrap();
    }
}

struct Space {
    /// for letting matrix users join
    public_room: Room,
    /// for letting bridge users join
    internal_room: Room,
    /// map discord channels to matrix rooms
    rooms: HashMap<ChannelId, Room>,
}

impl Space {
    fn get_join_rules(&self) -> join_rules::JoinRule {
        let external =
            join_rules::AllowRule::room_membership(self.public_room.room.room_id().into());
        let internal =
            join_rules::AllowRule::room_membership(self.internal_room.room.room_id().into());
        join_rules::JoinRule::Restricted(join_rules::Restricted::new(vec![external, internal]))
    }

    pub async fn setup(appservice: &AppService, user: Client, bridge: &Bridge) -> Self {
        let admin = bridge.space.admin.as_deref();
        let moderator = bridge.space.moderator.as_deref();

        let mut space = Space {
            public_room: Room::setup(
                appservice,
                user.clone(),
                &bridge.space.public_space,
                bridge.space.name.clone(),
                admin,
                moderator,
                Some(&join_rules::JoinRule::Public),
                true,
            )
            .await,
            internal_room: Room::setup(
                appservice,
                user.clone(),
                &bridge.space.internal_space,
                format!("{} Internal", bridge.space.name),
                admin,
                moderator,
                None,
                true,
            )
            .await,
            rooms: HashMap::new(),
        };
        let join_rules = space.get_join_rules();
        for relay in bridge.relays.iter() {
            let room = Room::setup(
                appservice,
                user.clone(),
                &relay.room,
                relay.name.clone(),
                admin,
                moderator,
                Some(&join_rules),
                false,
            )
            .await;
            if room.created {
                // setup space relationship
                let mut child = space::child::SpaceChildEventContent::new();
                child.order = Some(relay.ordering.clone());
                child.via = Some(vec![user.user_id().unwrap().server_name().to_owned()]);
                child.suggested = true;
                space
                    .public_room
                    .room
                    .send_state_event_for_key(room.room.room_id(), child.clone())
                    .await
                    .unwrap();
                space
                    .internal_room
                    .room
                    .send_state_event_for_key(room.room.room_id(), child)
                    .await
                    .unwrap();

                let parent = space::parent::SpaceParentEventContent::new(true);
                room.room
                    .send_state_event_for_key(space.public_room.room.room_id(), parent)
                    .await
                    .unwrap();
            }
            space.rooms.insert(relay.channel_id, room);
        }
        space
    }

    pub async fn on_discord_event(&mut self, event: DiscordEvent) {
        if let Some(room) = self.rooms.get_mut(&event.msg.channel_id) {
            room.on_discord_event(event).await;
        }
    }
}

struct MatrixInternal {
    spaces: HashMap<GuildId, Space>,
}

impl MatrixInternal {
    pub async fn setup(appservice: &AppService, user: &Client, bridges: &[Bridge]) -> Self {
        let mut matrix = MatrixInternal {
            spaces: HashMap::new(),
        };
        for bridge in bridges {
            let space = Space::setup(appservice, user.clone(), bridge).await;
            matrix.spaces.insert(bridge.space.guild_id, space);
        }
        matrix
    }

    pub async fn forward_discord_messages(
        &mut self,
        mut rx: mpsc::UnboundedReceiver<DiscordEvent>,
    ) {
        while let Some(event) = rx.recv().await {
            warn!("on_msg matrix {:?}", event.msg);
            if let Some(guild_id) = event.msg.guild_id {
                if let Some(space) = self.spaces.get_mut(&guild_id) {
                    space.on_discord_event(event).await;
                } else {
                    warn!(
                        "got discord event for unknown space: {guild_id} {:?}",
                        event.msg
                    );
                }
            } else {
                warn!("unprocessed discord event: {:?}", event.msg);
            }
        }
        error!("on_msg matrix finished");
    }
}

#[derive(Clone)]
struct MatrixListener {
    appservice: AppService,
    tx: mpsc::UnboundedSender<MatrixEvent>,
}

impl MatrixListener {
    pub fn new(appservice: AppService, tx: mpsc::UnboundedSender<MatrixEvent>) -> Self {
        Self { appservice, tx }
    }
}

impl Matrix {
    pub fn new(config: MatrixConfig) -> Self {
        Matrix { config }
    }

    async fn forward_discord_messages(
        mut matrix: MatrixInternal,
        rx: mpsc::UnboundedReceiver<DiscordEvent>,
    ) -> Result<(), ()> {
        matrix.forward_discord_messages(rx).await;
        Err(())
    }

    async fn listen_matrix(
        appservice: &AppService,
        tx: mpsc::UnboundedSender<MatrixEvent>,
    ) -> Result<(), ()> {
        appservice
            .register_user_query(Box::new(|appservice, req| {
                Box::pin(async move {
                    println!("Got request for {}", req.user_id);
                    // TODO: lookup userid in HashMap
                    true
                })
            }))
            .await;

        let matrix_listener = MatrixListener::new(appservice.clone(), tx);
        let bot = appservice.user(None).await.unwrap();

        bot.add_event_handler_context(matrix_listener);
        bot.add_event_handler(
            move |ev: SyncRoomMessageEvent,
                  room: matrix_sdk::room::Room,
                  Ctx(ml): Ctx<MatrixListener>| {
                async move {
                    println!("message {:?}", ev);
                    if let Some(msg) = ev.as_original() {
                        if ml.appservice.user_id_is_in_namespace(&msg.sender) {
                            println!("discarding own message");
                            return;
                        }
                        let content = msg.content.body().to_string();
                        let owner_name = msg.sender.as_str().to_string();
                        let matrix_event = MatrixEvent {
                            channel_id: 1062725162753069149,
                            webhook: Box::new(move |w| w.username(&owner_name).content(&content)),
                        };
                        ml.tx.send(matrix_event).unwrap();
                    }
                }
            },
        );

        let (host, port) = appservice.registration().get_host_and_port().unwrap();
        appservice.run(host, port).await.unwrap();
        Err(())
    }

    pub async fn start_bridge(
        &mut self,
        bridges: &[Bridge],
        rx: mpsc::UnboundedReceiver<DiscordEvent>,
        tx: mpsc::UnboundedSender<MatrixEvent>,
    ) -> Result<(), ()> {
        let registration =
            AppServiceRegistration::try_from_yaml_file(&self.config.appservice_registration_file)
                .unwrap();
        let appservice = AppService::builder(
            self.config.homeserver_url.clone(),
            self.config.server_name.clone(),
            registration,
        )
        .build()
        .await
        .unwrap();

        let _ = appservice.register_user("_discord_bot", None).await;
        let user = UserBuilder::new(&appservice, "_discord_bot")
            .device_id(Some("mad-rs".into()))
            .login()
            .build()
            .await
            .unwrap();
        let matrix = MatrixInternal::setup(&appservice, &user, bridges).await;

        let task1 = spawn(async move { Self::forward_discord_messages(matrix, rx).await });
        let task2 = spawn(async move { Self::listen_matrix(&appservice, tx).await });

        select!(
            result = task1 => {
                result.unwrap().unwrap();
            }
            result = task2 => {
                result.unwrap().unwrap();
            }
        );
        Err(())
    }
}
